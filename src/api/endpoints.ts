export const Endpoints = {
    AUTH: {
        LOGIN: 'https://lab.lectrum.io/js2/api/ironbank/login',
        REGISTER: 'https://lab.lectrum.io/js2/api/ironbank/register',
        AUTH: 'https://lab.lectrum.io/js2/api/ironbank/auth',
        ISSUE: 'https://lab.lectrum.io/js2/api/ironbank/cards',
        PROFILE: 'https://lab.lectrum.io/js2/api/ironbank/profile',
        CARDS: 'https://lab.lectrum.io/js2/api/ironbank/cards',
    },
};
