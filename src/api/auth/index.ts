/* eslint-disable max-len */
import { AxiosPromise } from 'axios';
import { Endpoints } from '../endpoints';
import { axiosInstance } from '../instance';
import {
  CardOperationsRequest,
    ICheckTokenRequest, IIssueCardRequestBody, IIssueCardRequestConfig, ILoginRequest, ILoginResponse, IRegisterRequest,
} from './type';

export const loginFn = (params: ILoginRequest): AxiosPromise<ILoginResponse> => axiosInstance.post(Endpoints.AUTH.LOGIN, params);

export const registerFn = (params: IRegisterRequest): AxiosPromise<ILoginResponse> => axiosInstance.post(Endpoints.AUTH.REGISTER, params);

export const checkTokenFn = (params: ICheckTokenRequest): AxiosPromise => axiosInstance.get(Endpoints.AUTH.AUTH, params);

export const issueCardFn = (body: IIssueCardRequestBody, config: IIssueCardRequestConfig): AxiosPromise => axiosInstance.post(Endpoints.AUTH.ISSUE, body, config);

export const getProfileFn = (params: ICheckTokenRequest): AxiosPromise => axiosInstance.get(Endpoints.AUTH.PROFILE, params);

export const getCardsFn = (params: ICheckTokenRequest): AxiosPromise => axiosInstance.get(Endpoints.AUTH.CARDS, params);

export const getOperationsFn = (params: CardOperationsRequest): AxiosPromise => axiosInstance.get(Endpoints.AUTH.CARDS, params);
