export interface ILoginRequest {
  email: string;
  password: string;
}

export interface ICheckTokenRequest {
  headers: { "x-token": string };
}

export interface IRegisterSchema {
  name: string;
  email: string;
  phone: string;
  password: string;
  terms: boolean;
}

export interface IRegisterRequest {
  name: string;
  email: string;
  phone: string;
  password: string;
}
export interface IIssueCardRequestBody {
  issuer: string;
  system: string;
  currency: string;
  class: string;
  description?: string;
}
export interface IIssueCardRequestConfig {
  headers: {
    "x-token": string;
  };
}

export interface ILoginResponse {
  data: {
    accessToken: string;
  };
}

export interface CardOperationsRequest {
  headers: {
    "x-token": string;
  };
  path: string;
}
