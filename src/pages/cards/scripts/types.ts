export interface Card {
  hash: string;
  issuer: string;
  system: string;
  iban: string;
  currency: string;
  class: string;
  card: string;
  description: string;
  valid: string;
  balance: number;
  limit: number;
  internet: boolean;
  security3d: boolean;
}

export interface Transaction {
  hash: string;
  title: string;
  description: string;
  operation: string;
  amount: number;
  card: string;
  cur: string;
  created: string;
}
