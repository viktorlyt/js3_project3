import axios from "axios";
import io from "socket.io-client";
import toastr from "toastr";
import { getCardsFn, getProfileFn } from "../../../api/auth";
import { axiosInstance } from "../../../api/instance";
// import { axiosInstance } from '../../../api/instance';
import "../../../css/style.scss";
import { Card, Transaction } from "./types";

const socket = io("https://lab.lectrum.io", {
  path: "/ws",
});

socket.on("connect", () => {
  console.log("connected");
  toastr.info("connected");
});

socket.on("disconnect", () => {
  console.log("disconnect");
});

// get profile & cards

let cards: Card[] = [];

const token = localStorage.getItem("token");
if (token) {
  getProfileFn({ headers: { "x-token": `${JSON.parse(token)}` } })
    .then((res) => {
      // console.log(res.data.data['name']);
      localStorage.setItem("profile", JSON.stringify(res.data.data));
      const userName = document.getElementById("name");
      if (userName) userName.innerText = res.data.data.name;

      return res.data;
    })
    .catch((error) => toastr.error(error.message));

  getCardsFn({ headers: { "x-token": `${JSON.parse(token)}` } })
    .then((res) => {
      localStorage.setItem("cards", JSON.stringify(res.data.data));

      return res.data;
    })
    .catch((error) => toastr.error(error.message));
}

const profileFromLocal = localStorage.getItem("profile");
let profile;
if (profileFromLocal) {
  profile = JSON.parse(profileFromLocal);
}

// socket!!!!!!!!!!!!!!!!!!!!!!!!!
socket.emit("login", `ironbank:${profile.email}`);
socket.on("login", (data) => {
  console.log(data);
  toastr.info(data);
});
socket.on("notification", (source) => {
  const data = JSON.parse(source);

  toastr.info(data);
});

//! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

function getCard(card: Card) {
  const typeOfCard =
    card.system === "mastercard" ? "mastercard-icon.svg" : "visa-icon.svg";
  const monthFromCard = (new Date(card.valid).getMonth() + 1).toString();
  const month =
    monthFromCard.length === 1 ? `0${monthFromCard}` : monthFromCard;
  const year = new Date(card.valid).getFullYear().toString().slice(2);
  const creditLimit = card.limit === 0 ? "Отсутствует" : card.limit;

  return `
    <div class="card__info__item collapsed">
    <div class="card__info__header">
        <img src="img/icon/${typeOfCard}" alt="" class="card__info__icon">
        <p class="card__info__data">Личная карта ** ${card.card.slice(12)}</p>
        <p class="card__info__balance">$${card.balance}</p>
        <img src="img/icon/arr-bottom.svg" alt="" class="arr__icon">
    </div>
    <div class="card__info__main">
        <div class="card__info__card">
            <img src="img/card-back.jpg" class="card__back" alt="">
            <img src="img/icon/${typeOfCard}" class="card__type" alt="">
            <p class="card__number">${card.card.slice(0, 4)} ${card.card.slice(
    4,
    8
  )} ${card.card.slice(8, 12)} ${card.card.slice(12, 16)}</p>
            <p class="card__owner__name">${card.issuer}</p>
            <p class="card__exp__date">${month} / ${year}</p>
        </div>
        <div class="card__info__settings">
            <div class="data-item">
                <p class="legend">Класс карты</p>
                <p class="data">${
                  card.system[0].toUpperCase() + card.system.slice(1)
                } ${card.class[0].toUpperCase() + card.class.slice(1)}</p>
            </div>
            <div class="data-item">
                <p class="legend">IBAN-номер</p>
                <p class="data">${card.iban}</p>
            </div>
            <div class="data-item">
                <p class="legend">Кредитный лимит</p>
                <p class="data">${creditLimit}</p>
            </div>
            <div class="data-item">
                <p class="legend">Покупки в интернете</p>
                <!-- Для того что бы "включить switch нужно добавить к классу switch__block класс checked" -->
                <div class="switch__block internet__switch__block checked">
                    <div class="switcher "></div>
                    <input type="checkbox" id="internetPayments">
                </div>
            </div>
            <div class="data-item">
                <p class="legend">3D Security</p>
                <!-- Для того что бы "включить switch нужно добавить к классу switch__block класс checked" -->
                <div class="switch__block security__switch__block  ">
                    <div class="switcher "></div>
                    <input type="checkbox" id="security3D">
                </div>
            </div>
            <div class="other__settings">
                <img class="settings__icon" src="img/icon/dots-icon.svg" alt="">
                <p class="settings__name">Операции над картой</p>
            </div>
        </div>
    </div>
</div>`;
}

const generateHTML = (cards: Card[]) => {
  const cards__info__layout = document.getElementsByClassName(
    "cards__info__layout"
  )[0];

  cards__info__layout.innerHTML = "";
  const cards__info__layoutHTML = `${cards
    .map((card: Card) => {
      return getCard(card);
    })
    .join(
      ""
    )}<a href="issue-card.html" target="_blank" class="create__card__button">Выпустить новую карту</a>`;

  cards__info__layout.innerHTML = cards__info__layoutHTML;
};

const cardsFromLocal = localStorage.getItem("cards");
if (cardsFromLocal) {
  cards = JSON.parse(cardsFromLocal);
}
generateHTML(cards);
// setTimeout(() => {
//   generateHTML(cards);
// }, 500);

const card__info__item = Array.from(
  document.querySelectorAll(".card__info__item")
);
console.log(card__info__item);

card__info__item.forEach((card) => {
  card.addEventListener("click", () => {
    card__info__item.forEach((el) => {
      el.classList.add("collapsed");
      el.classList.remove("active_card");
    });
    card.classList.remove("collapsed");
    card.classList.add("active_card");
    const cardNumber = document
      .querySelector("div.active_card p[class='card__number']")
      ?.textContent?.split(" ")
      .join("");
      console.log(cardNumber)
    if (cardNumber) getOperations(cardNumber);
  });
});

// get operations

function getOperations(cardNumber: string) {
  const token = localStorage.getItem("token");
  if (token) {
    axiosInstance
      .get(`https://lab.lectrum.io/js2/api/ironbank/orders/${cardNumber}`, {
        headers: { "x-token": `${JSON.parse(token)}` },
      })
      .then((res) => {
        console.log(res.data.data);
        generateTransactionHTML(res.data.data);
        localStorage.setItem(`${cardNumber}`, JSON.stringify(res.data.data));
      })
      .catch((error) => toastr.error(error));
  }
}

// change preload to transactions

setTimeout(() => {
  const completedBlock = document.querySelector(
    "article[data-status=completed]"
  );
  const downloadBlock = document.querySelector("article[data-status=download]");
  downloadBlock?.setAttribute("style", "display: none");
  completedBlock?.removeAttribute("style");
  console.log(completedBlock);
}, 2000);

function getTransactionHTML(transaction: Transaction) {
  const operation = transaction.operation === 'debet' ? '">-' : 'increase">+ '
  return `
  <div class="transaction__item">
    <img
      class="transaction__item__icon"
      src="img/transaction/money-pig.svg"
      alt=""
    />
    <div>
      <p class="transaction__item__type">
        ${transaction.title}
      </p>
      <p class="transaction__item__data">${transaction.description}</p>
    </div>
    <div class="transaction__item__value ${operation} $${transaction.amount}</div>
    <img
      src="img/icon/dots-icon.svg"
      class="transaction__item__more"
      alt=""
    />
  </div>`;
}

function generateTransactionHTML(transactions: Transaction[]){
  const transaction__main = document.getElementsByClassName('transaction__main overflow')[0];
  console.log(transaction__main)
  transaction__main.innerHTML = '';
  const transaction__mainHTML = transactions.map(transaction => getTransactionHTML(transaction)).join('');
  transaction__main.innerHTML = transaction__mainHTML;
}

const activeUser1 = {
  issuer: 'Peter Smetannikov',
  system: 'visa',
  currency: 'uah',
  class: 'gold',
  description: 'краткое описание карты',
};

axios({
  method: 'POST',
  url: 'https://lab.lectrum.io/js2/api/ironbank/cards',
  data: activeUser1,
  headers: { 'x-token': token },
})
  .then(function (response) {
    console.log('response.data.name', response.data);
  });