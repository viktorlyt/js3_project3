/* eslint-disable no-nested-ternary */
import toastr from 'toastr';
import { issueCardFn } from '../../../api/auth';
import '../../../css/style.scss';

// dropdown cards
const arr_bottom_card = <HTMLInputElement>(
  document.getElementById('arr_bottom_card')
);
const dropdown_card = <HTMLInputElement>(
  document.getElementById('dropdown_card')
);
const dropdown__toggle_card = <HTMLInputElement>(
  document.getElementById('dropdown__toggle_card')
);
const visa_card = document.getElementById('visa_card');
const master_card = document.getElementById('master_card');
const platinum_v = document.getElementById('platinum-v');
const world = document.getElementById('world');
const signature = document.getElementById('signature');
const platinum_m = document.getElementById('platinum-m');
const elite = document.getElementById('elite');
const infinite = document.getElementById('infinite');
const issue_card_form = document.getElementById('issue_card_form');

arr_bottom_card.addEventListener('click', () => {
    dropdown_card?.classList.add('dropdown--open');
    dropdown__toggle_card?.classList.add('active');
});
visa_card?.addEventListener('click', () => {
    dropdown_card?.classList.remove('dropdown--open');
    dropdown__toggle_card?.classList.remove('active');
    dropdown__toggle_card.innerHTML = `
    <img src="img/icon/visa-icon.svg" class="paysystem__icon visa__icon" alt="">
    <input class="form__input" type="text" id="paySystem" placeholder="Виза" readonly="">
    `;
    dropdown__toggle_card.append(arr_bottom_card);
    world?.setAttribute('style', 'display: none');
    platinum_m?.setAttribute('style', 'display: none');
    elite?.setAttribute('style', 'display: none');
    platinum_v?.removeAttribute('style');
    signature?.removeAttribute('style');
    infinite?.removeAttribute('style');
});
master_card?.addEventListener('click', () => {
    dropdown_card?.classList.remove('dropdown--open');
    dropdown__toggle_card?.classList.remove('active');
    dropdown__toggle_card.innerHTML = `
    <img src="img/icon/mastercard-icon.svg" class="paysystem__icon mastercard__icon" alt="">
    <input class="form__input" type="text" id="paySystem" placeholder="Mastercard" readonly="">
    `;
    dropdown__toggle_card.append(arr_bottom_card);
    platinum_v?.setAttribute('style', 'display: none');
    signature?.setAttribute('style', 'display: none');
    infinite?.setAttribute('style', 'display: none');
    platinum_m?.removeAttribute('style');
    world?.removeAttribute('style');
    elite?.removeAttribute('style');
});

// dropdown currencies

const arr_bottom_currency = <HTMLInputElement>(
  document.getElementById('arr_bottom_currency')
);
const dropdown_currency = <HTMLInputElement>(
  document.getElementById('dropdown_currency')
);
const dropdown__toggle_currency = <HTMLInputElement>(
  document.getElementById('dropdown__toggle_currency')
);
const usd = document.getElementById('usd');
const euro = document.getElementById('euro');
const uah = document.getElementById('uah');
const rub = document.getElementById('rub');

arr_bottom_currency.addEventListener('click', () => {
    dropdown_currency?.classList.add('dropdown--open');
    dropdown__toggle_currency?.classList.add('active');
});
usd?.addEventListener('click', () => {
    dropdown_currency?.classList.remove('dropdown--open');
    dropdown__toggle_currency?.classList.remove('active');
    dropdown__toggle_currency.innerHTML = `
      <input class="form__input" type="text"
        id="currency" placeholder="Доллар" readonly=""
      />
    `;
    dropdown__toggle_currency.append(arr_bottom_currency);
});
euro?.addEventListener('click', () => {
    dropdown_currency?.classList.remove('dropdown--open');
    dropdown__toggle_currency?.classList.remove('active');
    dropdown__toggle_currency.innerHTML = `
      <input class="form__input" type="text"
        id="currency" placeholder="Евро" readonly=""
      />
    `;
    dropdown__toggle_currency.append(arr_bottom_currency);
});
uah?.addEventListener('click', () => {
    dropdown_currency?.classList.remove('dropdown--open');
    dropdown__toggle_currency?.classList.remove('active');
    dropdown__toggle_currency.innerHTML = `
      <input class="form__input" type="text"
        id="currency" placeholder="Гривна" readonly=""
      />
    `;
    dropdown__toggle_currency.append(arr_bottom_currency);
});
rub?.addEventListener('click', () => {
    dropdown_currency?.classList.remove('dropdown--open');
    dropdown__toggle_currency?.classList.remove('active');
    dropdown__toggle_currency.innerHTML = `
      <input class="form__input" type="text"
        id="currency" placeholder="Рубли" readonly=""
      />
    `;
    dropdown__toggle_currency.append(arr_bottom_currency);
});

// form submit

issue_card_form?.addEventListener('submit', (event: Event) => {
    event.preventDefault();
    const cardType = <HTMLInputElement>document.getElementById('cardType');
    const issuer = cardType.value;
    const paySystem = <HTMLInputElement>document.getElementById('paySystem');
    const system = paySystem.placeholder === 'Виза' ? 'visa' : 'mastercard';
    const currencyEl = <HTMLInputElement>document.getElementById('currency');
    const currency
    = currencyEl.placeholder === 'Доллар'
        ? 'usd'
        : currencyEl.placeholder === 'Евро'
            ? 'eur'
            : currencyEl.placeholder === 'Гривна'
                ? 'uah'
                : 'rub';
    const cardClassesSelected = [
    <HTMLInputElement>document.getElementById('low'),
    <HTMLInputElement>document.getElementById('medium'),
    <HTMLInputElement>document.getElementById('high'),
    ].filter((el) => el.checked)[0];
    const mediumClass
    = Array.from(document.querySelectorAll('label[for=medium]')).filter((el) => el.hasAttribute('style'))[0].id === 'world'
        ? 'platinum'
        : 'world';
    const highClass
    = Array.from(document.querySelectorAll('label[for=high]')).filter((el) => el.hasAttribute('style'))[0].id === 'signature'
        ? 'platinum'
        : 'signature';
    const classCard
    = cardClassesSelected.id === 'low'
        ? 'gold'
        : cardClassesSelected.id === 'medium'
            ? mediumClass
            : highClass;
    const cardDescr = <HTMLInputElement>document.getElementById('cardDescr');
    const description = cardDescr.value;
    const token = localStorage.getItem('token');

    if (token) {
        const body = {
            issuer,
            system,
            currency,
            class: classCard,
            description,
        };
        const config = {
            headers: { 'x-token': JSON.parse(token) },
        };

        issueCardFn(body, config)
            .then((res) => {
                // window.location.href = 'cards.html';

                return res.data;
            })
            .catch((error) => toastr.error(error.message));
    }
});
