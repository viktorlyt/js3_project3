const arr_bottom_flat = <HTMLInputElement>(
  document.getElementById("arr_bottom_flat")
);
const dropdown_flat = <HTMLInputElement>(
  document.getElementById("dropdown_flat")
);
const dropdown__toggle_flat = <HTMLInputElement>(
  document.getElementById("dropdown__toggle_flat")
);
const arr_bottom_communal = <HTMLInputElement>(
  document.getElementById("arr_bottom_communal")
);
const dropdown_communal = <HTMLInputElement>(
  document.getElementById("dropdown_communal")
);
const dropdown__toggle_communal = <HTMLInputElement>(
  document.getElementById("dropdown__toggle_communal")
);

arr_bottom_flat.addEventListener("click", () => {
  dropdown_flat?.classList.add("dropdown--open");
  dropdown__toggle_flat?.classList.add("active");
});

arr_bottom_communal.addEventListener("click", () => {
  dropdown_communal?.classList.add("dropdown--open");
  dropdown__toggle_communal?.classList.add("active");
});

const flat1 = document.getElementById("flat1");
const flat2 = document.getElementById("flat2");
const communal_all = document.getElementById("communal_all");
const communal_water = document.getElementById("communal_water");
const communal_domophone = document.getElementById("communal_domophone");
const communal_ZHEK = document.getElementById("communal_ZHEK");
const communal_internet = document.getElementById("communal_internet");
const communal_security = document.getElementById("communal_security");
const communal_electricity = document.getElementById("communal_electricity");

function addEventListenerFn(
  domEl: HTMLElement | null,
  dropdown: HTMLInputElement,
  dropdown__toggle: HTMLInputElement,
  idText: string,
  innerText: string,
  arr_bottom: HTMLInputElement
) {
  domEl?.addEventListener("click", () => {
    dropdown?.classList.remove("dropdown--open");
    dropdown__toggle?.classList.remove("active");
    dropdown__toggle.innerHTML = `
    <div id="${idText}" class="dropdown__list__item">
      <p class="form__input">${innerText}</p>
    </div>
    `;
    dropdown__toggle.append(arr_bottom);
  });
}

addEventListenerFn(
  flat1,
  dropdown_flat,
  dropdown__toggle_flat,
  "flat1",
  "Квартира сына 1",
  arr_bottom_flat
);
addEventListenerFn(
  flat2,
  dropdown_flat,
  dropdown__toggle_flat,
  "flat2",
  "Квартира сына 2",
  arr_bottom_flat
);
addEventListenerFn(
  communal_all,
  dropdown_communal,
  dropdown__toggle_communal,
  "communal_all",
  "Все услуги",
  arr_bottom_communal
);
addEventListenerFn(
  communal_water,
  dropdown_communal,
  dropdown__toggle_communal,
  "communal_water",
  "Вода",
  arr_bottom_communal
);
addEventListenerFn(
  communal_domophone,
  dropdown_communal,
  dropdown__toggle_communal,
  "communal_all",
  "Домофон",
  arr_bottom_communal
);
addEventListenerFn(
  communal_ZHEK,
  dropdown_communal,
  dropdown__toggle_communal,
  "communal_all",
  "ЖЭК",
  arr_bottom_communal
);
addEventListenerFn(
  communal_internet,
  dropdown_communal,
  dropdown__toggle_communal,
  "communal_all",
  "Интернет",
  arr_bottom_communal
);
addEventListenerFn(
  communal_security,
  dropdown_communal,
  dropdown__toggle_communal,
  "communal_all",
  "Охрана",
  arr_bottom_communal
);
addEventListenerFn(
  communal_electricity,
  dropdown_communal,
  dropdown__toggle_communal,
  "communal_all",
  "Электричество",
  arr_bottom_communal
);

// flat1?.addEventListener('click', () => {
//   dropdown_flat?.classList.remove('dropdown--open');
//   dropdown__toggle_flat?.classList.remove('active');
//   dropdown__toggle_flat.innerHTML = `
//   <div id="flat1" class="dropdown__list__item">
//     <p class="form__input">Квартира сына 1</p>
//   </div>
//   `;
//   dropdown__toggle_flat.append(arr_bottom_flat);
// });

// flat2?.addEventListener('click', () => {
//   dropdown_flat?.classList.remove('dropdown--open');
//   dropdown__toggle_flat?.classList.remove('active');
//   dropdown__toggle_flat.innerHTML = `
//   <div id="flat2" class="dropdown__list__item">
//     <p class="form__input">Квартира сына 2</p>
//   </div>
//   `;
//   dropdown__toggle_flat.append(arr_bottom_flat);
// });
// communal_all?.addEventListener('click', () => {
//   dropdown_communal?.classList.remove('dropdown--open');
//   dropdown__toggle_communal?.classList.remove('active');
//   dropdown__toggle_communal.innerHTML = `
//   <div id="communal_all" class="dropdown__list__item">
//     <p class="form__input">Все услуги</p>
//   </div>
//   `;
//   dropdown__toggle_communal.append(arr_bottom_communal);
// });
// communal_water?.addEventListener('click', () => {
//   dropdown_communal?.classList.remove('dropdown--open');
//   dropdown__toggle_communal?.classList.remove('active');
//   dropdown__toggle_communal.innerHTML = `
//   <div id="communal_water" class="dropdown__list__item">
//     <p class="form__input">Вода</p>
//   </div>
//   `;
//   dropdown__toggle_communal.append(arr_bottom_communal);
// });
// communal_domophone?.addEventListener('click', () => {
//   dropdown_communal?.classList.remove('dropdown--open');
//   dropdown__toggle_communal?.classList.remove('active');
//   dropdown__toggle_communal.innerHTML = `
//   <div id="communal_domophone" class="dropdown__list__item">
//     <p class="form__input">Домофон</p>
//   </div>
//   `;
//   dropdown__toggle_communal.append(arr_bottom_communal);
// });
//   communal_ZHEK?.addEventListener('click', () => {
//   dropdown_communal?.classList.remove('dropdown--open');
//   dropdown__toggle_communal?.classList.remove('active');
//   dropdown__toggle_communal.innerHTML = `
//   <div id="communal_ZHEK" class="dropdown__list__item">
//     <p class="form__input">ЖЭК</p>
//   </div>
//   `;
//   dropdown__toggle_communal.append(arr_bottom_communal);
// });
//   communal_internet?.addEventListener('click', () => {
//   dropdown_communal?.classList.remove('dropdown--open');
//   dropdown__toggle_communal?.classList.remove('active');
//   dropdown__toggle_communal.innerHTML = `
//   <div id="communal_internet" class="dropdown__list__item">
//     <p class="form__input">Интернет</p>
//   </div>
//   `;
//   dropdown__toggle_communal.append(arr_bottom_communal);
// });
//   communal_security?.addEventListener('click', () => {
//   dropdown_communal?.classList.remove('dropdown--open');
//   dropdown__toggle_communal?.classList.remove('active');
//   dropdown__toggle_communal.innerHTML = `
//   <div id="communal_security" class="dropdown__list__item">
//     <p class="form__input">Охрана</p>
//   </div>
//   `;
//   dropdown__toggle_communal.append(arr_bottom_communal);
// });
//   communal_electricity?.addEventListener('click', () => {
//   dropdown_communal?.classList.remove('dropdown--open');
//   dropdown__toggle_communal?.classList.remove('active');
//   dropdown__toggle_communal.innerHTML = `
//   <div id="communal_electricity" class="dropdown__list__item">
//     <p class="form__input">Электричество</p>
//   </div>
//   `;
//   dropdown__toggle_communal.append(arr_bottom_communal);
// });

const switch__block = document.getElementsByClassName("switch__block")[0];
console.log(switch__block);
switch__block.addEventListener("click", () => {
  switch__block.classList.toggle("checked");
});

export {};
