export interface Transaction {
  hash: string;
  title: string;
  description: string;
  operation: string;
  amount: number;
  card: string;
  cur: string;
  created: string;
}
