import '../../../css/style.scss';
import io from 'socket.io-client';
import toastr from 'toastr';
import { axiosInstance } from '../../../api/instance';
import './communal.ts';

const socket = io('https://lab.lectrum.io', {
    path: '/ws',
});

socket.on('connect', () => {
    console.log('connected');
});

socket.on('disconnect', () => {
    console.log('disconnect');
});

socket.on('order', (data) => {
    toastr.info(JSON.parse(data));
});

const arr_bottom_card = <HTMLInputElement>(
  document.getElementById('arr_bottom_card')
);
const dropdown_card = <HTMLInputElement>(
  document.getElementById('dropdown_card')
);
const dropdown__toggle_card = <HTMLInputElement>(
  document.getElementById('dropdown__toggle_card')
);
const visa_card = document.getElementById('visa_card');
const master_card = document.getElementById('master_card');
const mobile = document.getElementById('mobile');
const utilityBills = document.getElementById('utilityBills');
const clientCard = document.getElementById('clientCard');
const ownCard = document.getElementById('ownCard');

const typesOfPaymentBlocks = [mobile, clientCard, utilityBills, ownCard];

arr_bottom_card.addEventListener('click', () => {
    dropdown_card?.classList.add('dropdown--open');
    dropdown__toggle_card?.classList.add('active');
});

visa_card?.addEventListener('click', () => {
    dropdown_card?.classList.remove('dropdown--open');
    dropdown__toggle_card?.classList.remove('active');
    dropdown__toggle_card.innerHTML = `
    <img src="img/icon/visa-icon.svg" class="paysystem__icon visa__icon" alt="">
    <input class="form__input" type="text" id="paySystem" placeholder="Виза" readonly="">
    `;
    dropdown__toggle_card.append(arr_bottom_card);
});

master_card?.addEventListener('click', () => {
    dropdown_card?.classList.remove('dropdown--open');
    dropdown__toggle_card?.classList.remove('active');
    dropdown__toggle_card.innerHTML = `
    <img src="img/icon/mastercard-icon.svg" class="paysystem__icon mastercard__icon" alt="">
    <input class="form__input" type="text" id="paySystem" placeholder="Mastercard" readonly="">
    `;
    dropdown__toggle_card.append(arr_bottom_card);
});

const typeOfPaymentInputs = Array.from(
    document.querySelectorAll('input[name=cardClass]'),
);
console.log(typeOfPaymentInputs);

const typeOfPayment = Array.from(document.querySelectorAll('.radio__button'));
console.log(typeOfPayment);

typeOfPayment.forEach((el) => {
    el.addEventListener('click', () => {
        const checkedType = typeOfPaymentInputs.filter(
            (item) => (item as HTMLInputElement).checked,
        )[0];
        console.log(checkedType);

        typesOfPaymentBlocks.forEach((el1) => el1?.setAttribute('style', 'display: none'),);
        if (checkedType.id === 'ownCardInput') ownCard?.removeAttribute('style');
        if (checkedType.id === 'otherCard') clientCard?.removeAttribute('style');
        if (checkedType.id === 'communal') utilityBills?.removeAttribute('style');
        if (checkedType.id === 'mobileInput') mobile?.removeAttribute('style');
    });
});

const mobileFillUp = document.getElementById('mobileFillUp');
mobileFillUp?.addEventListener('click', (e: Event) => {
  e.preventDefault();
  const contact = <HTMLInputElement>document.getElementById('contact');
  const contactTelNumber = <HTMLInputElement>document.getElementById('contactTelNumber');
  const totalSum = <HTMLInputElement>document.getElementById('totalSum');
  const description = contact.value || contactTelNumber.value;
  const title = 'Пополнение мобилки';
  const operation = 'debet';
  const amount = +totalSum.value;
  const card = '5587795840811306';
  const created = new Date();

  const token = localStorage.getItem('token');

  if (token) {
    const data = {
      title,
      description,
      operation,
      amount,
      card,
      created,
    };
    const config = {
        headers: { 'x-token': JSON.parse(token) },
    };

    axiosInstance.post(
      'https://lab.lectrum.io/js2/api/ironbank/orders',
      data, 
      config
    )
        .then((res) => {

            return res.data;
        })
        .catch((error) => toastr.error(error.message));
}
})
