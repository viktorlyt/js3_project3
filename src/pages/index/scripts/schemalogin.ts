// Core
import * as yup from 'yup';

// Elements
import {
    emailInputErrorLogin,
    passwordInputErrorLogin,
    email,
    password,
} from './elements';

// Types
import { FieldNamesLogin, IYupErrors } from './types';

export const schemaLogin = yup.object().shape({
    [FieldNamesLogin.email]: yup
        .string()
        .email('Email should be valid')
        .required('Required field'),
    [FieldNamesLogin.password]: yup.string().min(8).required('Required field'),
});

export const setValidationErrorsLogin = (errors: IYupErrors[]) => {
    for (const errorElement of errors) {
        const { path, message } = errorElement;
        if (
            path === FieldNamesLogin.email && email && emailInputErrorLogin
        ) {
            email.classList.add('form__input--error');
            emailInputErrorLogin.textContent = message;
        } else if (
            path === FieldNamesLogin.password && password && passwordInputErrorLogin
        ) {
            password.classList.add('form__input--error');
            passwordInputErrorLogin.textContent = message;
        }
    }
};

export const resetValidationErrorsLogin = () => {
    if (email && emailInputErrorLogin) {
        email.classList.remove('form__input--error');
        emailInputErrorLogin.textContent = '';
    }

    if (password && passwordInputErrorLogin) {
        password.classList.remove('form__input--error');
        passwordInputErrorLogin.textContent = '';
    }
};
