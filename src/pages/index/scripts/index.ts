/* eslint-disable @typescript-eslint/no-misused-promises */
import toastr from 'toastr';
import { checkTokenFn, loginFn, registerFn } from '../../../api/auth';
import {
    ILoginRequest,
    IRegisterRequest,
    IRegisterSchema,
} from '../../../api/auth/type';
import '../../../css/style.scss';
import {
    login,
    loginBtn,
    registerForm,
    registration,
    registrationBtn,
    registerName,
    registerEmail,
    registerTel,
    registerPassword,
    registerTerms,
    loginForm,
    email,
    password,
} from './elements';
import {
    resetValidationErrorsLogin,
    schemaLogin,
    setValidationErrorsLogin,
} from './schemalogin';
import {
    resetValidationErrors,
    schemaRegister,
    setValidationErrors,
} from './schemaregister';

loginBtn?.addEventListener('click', () => {
    login?.removeAttribute('style');
    registration?.setAttribute('style', 'display: none');
});
registrationBtn?.addEventListener('click', () => {
    registration?.removeAttribute('style');
    login?.setAttribute('style', 'display: none');
});

// register
registerForm?.addEventListener('submit', async (event: Event) => {
    event.preventDefault();

    const params: IRegisterSchema = {
        name: registerName.value,
        email: registerEmail.value,
        phone: registerTel.value,
        password: registerPassword.value,
        terms: registerTerms.checked,
    };

    const payload: IRegisterRequest = {
        name: registerName.value,
        email: registerEmail.value,
        phone: registerTel.value,
        password: registerPassword.value,
    };

    try {
        await schemaRegister.validate(params, { abortEarly: false });
    } catch (error) {
        if (Array.isArray(error.inner)) {
            setValidationErrors(error.inner);
        }

        return null;
    }

    registerFn(payload)
        .then((data) => {
            return data.data;
        })
        .then((data) => {
            localStorage.setItem('token', JSON.stringify(data.data));
            resetValidationErrors();
            toastr.info('You register successfully!', 'Thank you for registration.');
            window.location.href = 'cards.html';

            return data.data;
        })
        .catch((error) => toastr.error(error.message));
});

// login
loginForm?.addEventListener('submit', async (event: Event) => {
    event.preventDefault();

    const params: ILoginRequest = {
        email: email.value,
        password: password.value,
    };

    try {
        await schemaLogin.validate(params, { abortEarly: false });
    } catch (error) {
        if (Array.isArray(error.inner)) {
            // "useUnknownInCatchVariables": false in tsconfig
            setValidationErrorsLogin(error.inner);
        }

        return null;
    }

    loginFn(params)
        .then((data) => {
            return data.data;
        })
        .then((data) => {
            localStorage.setItem('token', JSON.stringify(data.data));
            resetValidationErrorsLogin();
            toastr.info('You login successfully!');
            window.location.href = 'cards.html';

            return data.data;
        })
        .catch((error) => toastr.error(error.message));
});

// checkToken
const token = localStorage.getItem('token');

if (token) {
    checkTokenFn({ headers: { 'x-token': `${JSON.parse(token)}` } })
        .then((res) => {
            if (res.status === 204) {
                window.location.href = 'cards.html';
            }

            return null;
        })
        .catch((error) => toastr.error(error.message));
}
