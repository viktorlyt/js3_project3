// register page

export const loginBtn = document.getElementById('loginBtn');
export const login = document.getElementById('login');
export const registrationBtn = document.getElementById('registrationBtn');
export const registration = document.getElementById('registration');
export const registerForm = document.getElementById('register');
export const registerName = <HTMLInputElement>document.getElementById('registerName');
export const registerEmail = <HTMLInputElement>document.getElementById('registerEmail');
export const registerTel = <HTMLInputElement>document.getElementById('registerTel');
export const registerPassword = <HTMLInputElement>document.getElementById('registerPassword');
export const registerTerms = <HTMLInputElement>document.getElementById('registerTerms');
export const registerTermsLabel = <HTMLInputElement>document.getElementById('registerTermsLabel');
export const nameInputError = document.getElementById('nameInputError');
export const emailInputError = document.getElementById('emailInputError');
export const telInputError = document.getElementById('telInputError');
export const passwordInputError = document.getElementById('passwordInputError');
export const termsInputError = document.getElementById('termsInputError');

// login page
export const loginForm = document.getElementById('loginForm');
export const email = <HTMLInputElement>document.getElementById('email');
export const password = <HTMLInputElement>document.getElementById('password');
export const emailInputErrorLogin = document.getElementById('emailInputErrorLogin');
export const passwordInputErrorLogin = document.getElementById('passwordInputErrorLogin');
