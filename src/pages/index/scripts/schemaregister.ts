// Core
import * as yup from 'yup';

// Elements
import {
    emailInputError,
    nameInputError,
    passwordInputError,
    registerEmail,
    registerName,
    registerPassword,
    registerTel,
    registerTermsLabel,
    telInputError,
    termsInputError,
} from './elements';

// Types
import { FieldNamesRegister, IYupErrors } from './types';

export const schemaRegister = yup.object().shape({
    [FieldNamesRegister.name]: yup.string().min(2).max(20).required('Required field'),
    [FieldNamesRegister.email]: yup
        .string()
        .email('Email should be valid')
        .required('Required field'),
    [FieldNamesRegister.phone]: yup.string().matches(/\+\d{10,13}/).required('Required field'),
    [FieldNamesRegister.password]: yup.string().min(8).required('Required field'),
    [FieldNamesRegister.terms]: yup.bool().default(false).oneOf([true], 'You must accept the terms and conditions').required('You must accept the terms and conditions'),
});

export const setValidationErrors = (errors: IYupErrors[]) => {
    for (const errorElement of errors) {
        const { path, message } = errorElement;
        if (path === FieldNamesRegister.name && nameInputError && registerName) {
            registerName.classList.add('form__input--error');
            nameInputError.textContent = message;
        } else if (
            path === FieldNamesRegister.email && registerEmail && emailInputError
        ) {
            registerEmail.classList.add('form__input--error');
            emailInputError.textContent = message;
        } else if (
            path === FieldNamesRegister.phone && registerTel && telInputError
        ) {
            registerTel.classList.add('form__input--error');
            telInputError.textContent = message;
        } else if (
            path === FieldNamesRegister.password && registerPassword && passwordInputError
        ) {
            registerPassword.classList.add('form__input--error');
            passwordInputError.textContent = message;
        } else if (
            path === FieldNamesRegister.terms && registerTermsLabel && termsInputError
        ) {
            registerTermsLabel.classList.add('form__label--error');
            termsInputError.textContent = message;
        }
    }
};

export const resetValidationErrors = () => {
    if (nameInputError && registerName) {
        registerName.classList.remove('form__input--error');
        nameInputError.textContent = '';
    }

    if (registerEmail && emailInputError) {
        registerEmail.classList.remove('form__input--error');
        emailInputError.textContent = '';
    }

    if (registerTel && telInputError) {
        registerTel.classList.remove('form__input--error');
        telInputError.textContent = '';
    }

    if (registerPassword && passwordInputError) {
        registerPassword.classList.remove('form__input--error');
        passwordInputError.textContent = '';
    }

    if (registerTermsLabel && termsInputError) {
        registerTermsLabel.classList.remove('form__input--error');
        termsInputError.textContent = '';
    }
};
