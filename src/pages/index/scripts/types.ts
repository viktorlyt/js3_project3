export enum FieldNamesRegister {
    name = 'name',
    email = 'email',
    phone = 'phone',
    password = 'password',
    terms = 'terms',
}

export enum FieldNamesLogin {
    email = 'email',
    password = 'password',
}

export interface IYupErrors {
    path: string;
    message: string;
}
